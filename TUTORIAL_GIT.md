# TUTORIAL GIT

## 1. CONFIGURACIÓN

Muestra la configuración actual

$ `git config --list`

Configuración de identidad (para todos los repositorios locales)

$ `git config --global user.name "John Doe"`

$ `git config --global user.email johndoe@example.com`

Configuración de identidad (para un repositorio local en especifico)

$ `git config user.name "John Doe"`

$ `git config user.email johndoe@example.com`

## 2. CLONAR UN REPOSITORIO REMOTO (creamos un repositorio local)

Si utilizas github:
- $ `git clone https://github.com/username/my-project.git` [HTTPS]
- $ `git clone git@github.com:username/my-project.git` [SSH]

Si utilizas gitlab:
- $ `git clone https://gitlab.com/username/my-project.git` [HTTPS]
- $ `git clone git@gitlab.com:username/my-project.git` [SSH]

## 3. CONECTARSE A UN REPOSITORIO REMOTO (ya tenemos un repositorio local)

Conectar tu repositorio local a un repositorio remoto

$ `git remote add origin git@github.com:username/my-project.git`

## 4. GUARDAR CAMBIOS (en el repositorio local)

Ver los cambios realizados

$ `git status`

Guardar un archivo específico

$ `git add README.md`

Guardar todos los archivos

$ `git add .`

Guardamos un mensaje que explica los cambios realizados

$ `git commit -am "File README.md created"`

## 5. GUARDAR CAMBIOS (en el repositorio remoto)

En la rama principal

$ `git push origin master`

En una rama secundaria

$ `git push origin my-branch`

#### Notas.-
- Las ramas son utilizadas para desarrollar funcionalidades aisladas unas de
otras.
- La rama master es la rama "por defecto" cuando creas un repositorio.
- Crea nuevas ramas durante el desarrollo y fusiónalas a la rama principal
cuando termines.

## 6. CREAR NUEVA RAMA

Muestra todas las ramas

$ `git branch`

Crea una nueva rama

$ `git branch --create my-branch`

Activa la rama my-branch

$ `git checkout my-branch`

Crea una nueva rama y se cambia a esta

$ `git checkout -b my-branch`

Elimina la rama testing

$ `git branch -d testing`

## 7. ACTUALIZAR Y FUSIONAR

Este comando recupera todos los datos del proyecto remoto que no tengas
todavía.

$ `git fetch origin`

Para actualizar tu repositorio local al commit más nuevo, ejecuta

$ `git pull`

Para fusionar otra rama a tu rama activa (por ejemplo master), utiliza

$ `git merge my-branch`

#### Notas-
- En ambos casos git intentará fusionar automáticamente los cambios.
- Desafortunadamente, no siempre será posible y se podrán producir conflictos.
- Tú eres responsable de fusionar esos conflictos manualmente al editar los
archivos mostrados por git.

Después de modificarlos, necesitas marcarlos como fusionados con

$ `git add <filename>`

Antes de fusionar los cambios, puedes revisarlos usando

$ `git diff <source_branch> <target_branch>`

## 8. DESHACER CAMBIOS

Deshacer todos los cambios locales y commits, traer la última versión del
servidor y apuntar a tu copia local principal.

$ `git fetch origin`

$ `git reset --hard origin/master`

Reemplazar cambios locales usando el comando

$ `git checkout -- <filename>`

## REFERENCIAS

- git - la guía sencilla
http://rogerdudler.github.io/git-guide/index.es.html

- gittutorial - A tutorial introduction to Git
https://git-scm.com/docs/gittutorial
